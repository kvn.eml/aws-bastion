
output "id" {
  description = "The ID of the instance"
  value       = element(concat(aws_instance.private_server.*.id, [""]), 0)
}

#output "arn" {
#  description = "The ARN of the instance"
#  value       = element(concat(aws_instance.this.*.arn, aws_spot_instance_request.this.*.arn, [""]), 0)
#}

#output "instance_state" {
#  description = "The state of the instance. One of: `pending`, `running`, `shutting-down`, `terminated`, `stopping`, `stopped`"
#  value       = element(concat(aws_instance.this.*.instance_state, aws_spot_instance_request.this.*.instance_state, [""]), 0)
#}

#output "primary_network_interface_id" {
#  description = "The ID of the instance's primary network interface"
#  value       = element(concat(aws_instance.this.*.primary_network_interface_id, aws_spot_instance_request.this.*.primary_network_interface_id, [""]), 0)
#}
