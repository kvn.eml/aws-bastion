
variable "instance_type" {
  default = "t2.micro"
}

variable "image_ami" {
  default = "ami-00dfe2c7ce89a450b"
}

variable "name" {
  description = "Bastion"
  type = string
  default = "Bastion"
}

variable "region" {
  default = "us-east-2"
}

 variable "vpc_id" {
   description = "Vpc id to setup bastion"
   type        = string
   default     = "vpc-0271d86436395c046"
}

variable "key_name" {
  type = string
  default     = "Name"
}
