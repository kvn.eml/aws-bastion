#!/bin/bash

[ -f network/.initialized.net ] || ( cd ./network && ./setup.sh )

[ -f .initialized ] || ( terraform init && touch ./.initialized )

terraform apply -auto-approve -var-file='variables.tfvars' -no-color | tee apply.log

instance=$( cat apply.log | grep "^id = " | cut -d'"' -f2 )
echo instance = $instance | tee -a ./apply.log

aws ssm start-session --target $instance 

