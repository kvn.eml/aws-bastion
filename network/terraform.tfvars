availability_zones = ["us-east-2a", "us-east-2b"]
name               = "bastion"
private_start_ip   = "30"
public_start_ip    = "20"
region             = "us-east-2"
subnet_prefix      = "10.0"
vpc_cidr           = "10.0.0.0/16"
