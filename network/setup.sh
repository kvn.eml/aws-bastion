#!/bin/bash

[ -f .initialized ] || terraform init && touch .initialized.net
terraform apply -auto-approve -var-file='terraform.tfvars'
