#!/bin/bash

terraform apply -destroy -auto-approve -var-file='variables.tfvars' | tee destroy.log

